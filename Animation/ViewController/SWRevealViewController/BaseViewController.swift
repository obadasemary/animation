//
//  BaseViewController.swift
//  Animation
//
//  Created by Abdelrahman Mohamed on 3/29/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        initNavicationItemView()
    }
    
    func initNavicationItemView() {
        
        if let vc = self.revealViewController() {
            
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "iconMenu")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(openLeft))
            
            self.view.addGestureRecognizer(vc.panGestureRecognizer())
            self.view.addGestureRecognizer(vc.tapGestureRecognizer())
            
            //            self.navigationController?.navigationBar.addGestureRecognizer(vc.panGestureRecognizer())
            //            self.navigationController?.navigationBar.addGestureRecognizer(vc.tapGestureRecognizer())
        }
    }
    
    @objc func openLeft() {
        
        let vc = self.revealViewController()
        vc?.revealToggle(animated: true)
    }
}
