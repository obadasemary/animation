//
//  SlideMenuViewController.swift
//  Animation
//
//  Created by Abdelrahman Mohamed on 3/29/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import UIKit

class SlideMenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    @IBAction func homeTouched(_ sender: UIButton) {
        
        callSlideMenu(identity: "welcomeVC")
    }
    
    @IBAction func animationTouched(_ sender: UIButton) {
        
        callSlideMenu(identity: "animationVC")
    }
    
    @IBAction func chartTouched(_ sender: UIButton) {
        
        callSlideMenu(identity: "chartVC")
    }
    
    @IBAction func walkthroughTouched(_ sender: UIButton) {
        
        callSlideMenu(identity: "walkthroughVC")
    }
    
    func callSlideMenu(identity: String) {
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: identity)
        
        let navC = UINavigationController(rootViewController: vc)
        
        if let reveal = self.revealViewController() {
            
            reveal.pushFrontViewController(navC, animated: true)
        }
    }
}
