//
//  CoursesViewController.swift
//  Ren2
//
//  Created by Abdelrahman Mohamed on 11/22/17.
//  Copyright © 2017 Abdelrahman Mohamed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SDWebImage

class CoursesViewController: BaseViewController, NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - shared
    
    static var shared = CoursesViewController()
    
    // MARK: - @IBOutlet

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - init
    
    let sizeNVActivityIndicator = CGSize(width: 30, height:30)
    
    var coursesList = [Course]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.startAnimating(self.sizeNVActivityIndicator, message: "Loading...", type: NVActivityIndicatorType.ballClipRotateMultiple)

        downloadURL()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func downloadURL() {
        
        let jsonUrlWebString = "https://api.letsbuildthatapp.com/jsondecodable/website_description"
        let jsonUrlString = "https://api.letsbuildthatapp.com/jsondecodable/courses"
        let jsonUrlSnakeCase = "https://api.letsbuildthatapp.com/jsondecodable/courses_snake_case"

        guard URL(string: jsonUrlWebString) != nil else { return }
        guard URL(string: jsonUrlString) != nil else { return }
        guard let urlSnakeCase = URL(string: jsonUrlSnakeCase) else { return }

//        URLSession.shared.dataTask(with: urlWeb) { (data, response, error) in
//
//            guard let data = data else { return }
//
//            do {
//
//                let decoder = JSONDecoder()
//                decoder.keyDecodingStrategy = .convertFromSnakeCase
//
//                let websiteDescription = try decoder.decode(WebsiteDescription.self, from: data)
//
//                self.coursesList = websiteDescription.courses
//
//                DispatchQueue.main.async {
//
//                    self.tableView.reloadData()
//                    self.stopAnimating()
//                }
//
//            } catch let jsonError {
//                print("Error serializing json:", jsonError)
//            }
//
//        }.resume()

//        URLSession.shared.dataTask(with: url) { (data, response, error) in
//
//            guard let data = data else { return }
//
//            do {
//
//                let decoder = JSONDecoder()
//                decoder.keyDecodingStrategy = .convertFromSnakeCase
//        
//                let courses = try decoder.decode([Course].self, from: data)
//
//                self.coursesList = courses
//
//                DispatchQueue.main.async {
//
//                    self.tableView.reloadData()
//                    self.stopAnimating()
//                }
//
//            } catch let jsonError {
//                print("Error serializing json:", jsonError)
//            }
//
//        }.resume()
        
//        URLSession.shared.dataTask(with: urlSnakeCase) { (data, response, error) in
//
//            guard let data = data else { return }
//
//            do {
//
//                let decoder = JSONDecoder()
//                decoder.keyDecodingStrategy = .convertFromSnakeCase
//
//                let courses = try decoder.decode([Course].self, from: data)
//
//                self.coursesList = courses
//
//                DispatchQueue.main.async {
//
//                    self.tableView.reloadData()
//                    self.stopAnimating()
//                }
//
//            } catch let jsonError {
//                print("Error serializing json:", jsonError)
//            }
//
//        }.resume()
        
        CoursesController.shared.getLastCourses(urlCourses: urlSnakeCase) { (success, courses) in
            
            if success {
                
                self.coursesList = courses!
                
                DispatchQueue.main.async {
                    
                    self.tableView.reloadData()
                    self.stopAnimating()
                }
            }
        }
        
//        CoursesController.shared.getCourses(urlCourses: urlSnakeCase) { (success, courses) in
//            
//            if success {
//                
//                self.coursesList = courses!
//                
//                DispatchQueue.main.async {
//                    
//                    self.tableView.reloadData()
//                    self.stopAnimating()
//                }
//            }
//        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return coursesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CoursesTableViewCell
        
        cell.name.text = coursesList[indexPath.row].name
        cell.link.text = String(coursesList[indexPath.row].numberOfLessons)
        
        cell.imageUrl.sd_setImage(with: URL(string: coursesList[indexPath.row].imageUrl), placeholderImage: UIImage(named: "Facto1.png"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
}

