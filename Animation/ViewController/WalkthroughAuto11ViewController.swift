//
//  WalkthroughAuto11ViewController.swift
//  Ren2
//
//  Created by Abdelrahman Mohamed on 12/6/17.
//  Copyright © 2017 Abdelrahman Mohamed. All rights reserved.
//

import UIKit

class WalkthroughAuto11ViewController: BaseViewController {
    
    let introImageView: UIImageView = {
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "Facto1"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let descriptionTextView: UITextView = {
        
        let textView = UITextView()
        textView.text = "Join us today in Ren2 Car"
        textView.font = UIFont.boldSystemFont(ofSize: 20)
        textView.textAlignment = .center
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(introImageView)
        view.addSubview(descriptionTextView)
        
        setupLayout()
    }
    
    func setupLayout() {
        
        introImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        introImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        introImageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        introImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        descriptionTextView.topAnchor.constraint(equalTo: introImageView.bottomAnchor, constant: 50).isActive = true
        descriptionTextView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
        descriptionTextView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
        descriptionTextView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
    }
}
