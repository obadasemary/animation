//
//  CoursesController.swift
//  Animation
//
//  Created by Abdelrahman Mohamed on 4/6/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import Foundation
import Alamofire

class CoursesController: NSObject {
    
    // MARK: - shared
    
    static let shared  = CoursesController()
    
    let decoder = JSONDecoder()
    
    // MARK: - getMovies
    
    func getCourses(urlCourses: URL, completion: @escaping (_ success: Bool, _ coursesResponse: [Course]?) -> Void ) {
        
        URLSession.shared.dataTask(with: urlCourses) { (data, response, error) in
            
            guard let data = data else { return }
            
            do {
                
                self.decoder.keyDecodingStrategy = .convertFromSnakeCase
                
                let courses = try self.decoder.decode([Course].self, from: data)
                
                completion(true, courses)
                
            } catch let jsonError {
                print("Error serializing json:", jsonError)
                completion(false, nil)
            }
            
        }.resume()
    }
    
    func getLastCourses(urlCourses: URL, completion: @escaping (_ success: Bool, _ coursesResponse: [Course]?) -> Void ) {
        
        var _: [String: Any]?
        
//        if (photo != nil) {
//
//            parameters = ["local": "ara", "user_id": userId, "access_token": accessToken, "name": name, "photo64": photo ?? "", "tags": """
//                {"tags": \(tags!)}
//                """] as [String : Any]
//
//        } else {
//
//            parameters = ["local": "ara", "user_id": userId, "access_token": accessToken, "name": name, "photo64": "", "tags": """
//                {"tags": \(tags!)}
//                """] as [String : Any]
//        }
        
//        print(tags)
        
        Alamofire.request(urlCourses, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            switch response.result {
                
            case .success:
                
                guard let data = response.data else { return }
                
                do {
                    
                    self.decoder.keyDecodingStrategy = .convertFromSnakeCase
                    
                    let courses = try self.decoder.decode([Course].self, from: data)
                    
                    completion(true, courses)
                    
                } catch let jsonError {
                    
                    print("Error serializing json:", jsonError)
                    completion(false, nil)
                }
                
            case .failure(let error):
                
                print(error)
                completion(false, nil)
            }
        }
    }
}
