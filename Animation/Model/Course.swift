//
//  Course.swift
//  Animation
//
//  Created by Abdelrahman Mohamed on 4/6/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import Foundation

struct WebsiteDescription: Decodable {
    
    let name: String
    let description: String
    let courses: [Course]
}

struct Course: Decodable {
    
    let id: Int
    let name: String
    let link: String
    let imageUrl: String
    let numberOfLessons: Int
}
